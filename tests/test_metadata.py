from qittnz6Lz.metadata import PipelineOutputWithMetadata, IFD_STRING_ENCODING


def _metadata_with_defaults(prompt, **kwargs):
    return PipelineOutputWithMetadata(
        prompts=[prompt],
        images=[],
        nsfw_content_detected=[],
        opposing_prompts=[],
        steps=10,
        scheduler="DDIM",
        scheduler_args={},
        height=512,
        width=512,
        software="TEST",
        seeds=[1]
    )


def test_ascii_prompt_as_exif():
    prompt = "Perfectly normal times."
    pipe_output = _metadata_with_defaults(prompt)
    exif_bytes = pipe_output.as_exif()
    assert prompt.encode(IFD_STRING_ENCODING) in exif_bytes


def test_unicode_prompt_as_exif():
    prompt = "The bane of 1995 file formats—the emdash. 😖"
    expected_prompt = "The bane of 1995 file formats--the emdash."
    pipe_output = _metadata_with_defaults(prompt)
    exif_bytes = pipe_output.as_exif()
    assert expected_prompt.encode(IFD_STRING_ENCODING) in exif_bytes
