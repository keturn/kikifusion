import json

import PIL.Image
import piexif
from piexif import ExifIFD
from piexif.helper import UserComment

from qittnz6Lz.engine import Engine
from qittnz6Lz.metadata import PipelineOutputWithMetadata


def _last_result(generator):
    result = None
    for result in generator:
        pass
    if result is None:
        raise RuntimeError("Expected a non-null result from the generator!")
    return result


def test_metadata_integration(tmp_path, request):
    engine = Engine(safety_check=False)
    engine.load()
    prompt = "This is only a test."
    opposing_prompt = "air traffic control"
    output: PipelineOutputWithMetadata = _last_result(engine.generate_from_text(
        prompt=prompt,
        opposing_prompt=opposing_prompt
    ))

    jpeg_paths = []

    def output_path(i):
        p = tmp_path / f"{request.node.name}-{i}.jpg"
        jpeg_paths.append(p)
        return p

    engine.save_output_image(output, output_path)
    assert len(jpeg_paths) == 1

    with open(jpeg_paths[0], 'rb') as path:
        image = PIL.Image.open(path, formats=('jpeg',))

    assert image.size == (512, 512)

    # TODO: this code should go in a Metadata.from_image method
    pil_exif: PIL.Image.Exif = image.getexif()
    exif = piexif.load(pil_exif.tobytes())
    # Why is the value a tuple instead of bytes???
    raw_comment = bytes(exif['Exif'][ExifIFD.UserComment])
    metadata_dict = json.loads(UserComment.load(raw_comment))
    assert metadata_dict['prompt'] == prompt
    assert metadata_dict['opposing_prompt'] == opposing_prompt
