= kikifusion

An interface for generation images from diffusion models.

== Development

After activating your Python environment, install the requirements:

[source,shell]
----
pip install -r dev-requirements.txt
----

Then build classes from the Qt ui files:

[source,shell]
----
doit compile-ui
----

Start the application:

[source,shell]
----
doit start
----

== Supporting Projects

A project like this is built on the prior work of many, many people.
Any attempt to list them all at once would be like one of those chapters that does nothing but recite the family tree,
or the end credits in a game with a thousand names;
sure to make your eyes glaze over a fraction of the way through.

But this image generation technology feels like _magic._
I think it's worth highlighting the sources of its most distinctive features:

* The Stable Diffusion and Latent Diffusion algorithms are the research product of what is now the Machine Vision & Learning group at the Ludwig Maximilian University of Munich.
Their paper https://ommer-lab.com/research/latent-diffusion-models/[High-Resolution Image Synthesis with Latent Diffusion Models] is available from their lab
or as https://doi.org/10.48550/arXiv.2112.10752[Rombach, Robin and Blattmann, Andreas and Lorenz, Dominik and Esser, Patrick and Ommer, Björn, _Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR),_ June 2022].
* The publicly released Stable Diffusion model was trained on a https://laion.ai/projects/[data set of _billions_ of image-text pairs collected by LAION].
* To interpret text and its relevance to images, Stable Diffusion uses the https://openai.com/blog/clip/[CLIP model from OpenAI].

Next, shout-outs to some smaller projects that helped make the development of this project easier in little ways.
Maybe knowing about them can make things easier for you too:

* Martin Fitzpatrick's https://www.pythonguis.com/[Python GUIs tutorials].
GUIs have been an oddly neglected topic in the Python ecosystem—
this site has been invaluable for navigating the current options and getting a UI like this started.
* After I had a numpy array of RGB pixel data, I thought it would be trivial to display.
_Surprisingly confusing,_ actually. Eventually I installed https://pypi.org/project/qimage2ndarray/[qimage2ndarray] and it handled it.
Thank you, Hans Meine.

== License

You may use, modify, and redistribute this software under the terms of the link:LICENSE.html[GNU GPL v3] yadda yadda.

I have to say that because the code is currently built on https://www.riverbankcomputing.com/software/pyqt/intro[PyQt5],
and the GPL is a viral license.

I reserve the right to re-license parts of this code that do not rely on PyQt.
