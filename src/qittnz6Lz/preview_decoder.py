import einops
import numpy as np
from PyQt5 import QtGui
from qimage2ndarray import array2qimage


class ApproximateDecoder:
    """Decodes latent data to an approximate representation in RGB.

    Values determined experimentally for Stable Diffusion 1.4.

    See https://discuss.huggingface.co/t/decoding-latents-to-rgb-without-upscaling/23204/2
    """

    # grayscale_factors = torch.tensor([
    #    #    R       G       B
    #    [ 0.342,  0.341,  0.343 ], # L1
    #    [ 0.342,  0.342,  0.340 ], # L2
    #    [-0.110, -0.110, -0.113 ], # L3
    #    [-0.208, -0.209, -0.208 ]  # L4
    # ])

    # latent_rgb_factors = np.array([
    #     #   R        G       B
    #     [ 0.298,  0.207,  0.208],  # L1
    #     [ 0.187,  0.286,  0.173],  # L2
    #     [-0.158,  0.189,  0.264],  # L3
    #     [-0.184, -0.271, -0.473],  # L4
    # ], dtype=dtype)

    def __init__(self, dtype: np.dtype = np.float16):
        # 4×4 matrix developed by hotgrits:
        #
        # > Alternate matrix for converting Stable Diffusion 1.4 latents
        # > to RGB + T* with T* being just an extra channel that seems to
        # > contain texture information. […] With this you can convert
        # > to RGB + Texture and back if you make the inverse matrix via
        # > torch.linalg.inv(matrix), unlike the 4 channels to RGB matrix.

        self.latent_rgbt_factors = np.array([
            [ 0.3444,  0.1385,  0.0670,  0.4838],
            [ 0.1247,  0.4027,  0.1494, -0.3709],
            [-0.3192,  0.2513,  0.2103,  0.1221],
            [-0.1307, -0.1874, -0.7445,  0.1484]
        ])
        self.latent_rgbt_factors.setflags(write=False)

    def __call__(self, latents: np.ndarray) -> tuple[QtGui.QImage, np.ndarray]:
        """Get an RGB JPEG representation of the latent data."""
        rgbt = np.einsum('...lhw,lr -> ...rhw', latents, self.latent_rgbt_factors)
        rgb = rgbt[..., :3, :, :]
        t = rgbt[..., 3:, :, :]
        rgb = einops.rearrange(rgb, "c h w -> h w c", c=3)
        t = einops.rearrange(t, "c h w -> h (w c)", c=1)
        return array2qimage(rgb, normalize=(-1, 1)), t
