import numpy as np
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import QTimer, QRegularExpression
from PyQt5.QtWidgets import QLabel
from diffusers.pipelines.stable_diffusion import StableDiffusionPipelineOutput
from qimage2ndarray import array2qimage
from scipy import ndimage

from ._features import has_pyqtgraph
from .constants import ENGINE
from .filters import laplace as laplace_1, standardize
from .generated.mainwindow import Ui_MainWindow
from .graphs import GraphWindow
from .preview_decoder import ApproximateDecoder
from .qengine import QEngine, IntermediateState
from .resource_monitor import MonitorView

# Adapted from https://krita.org/en/about/kiki/
# Note: This text should NOT be localized until we get localized pretrained models.
#     Stable Diffusion v1.4 is not great with non-English languages.
DEFAULT_PROMPT = """\
Kiki the Cyber Squirrel is mostly humanoid, with the features of a squirrel.
Splash art by Tyson Tan.
A robotic body with organic shapes represents tech with humanity.
White represents the color of paper.
Pink represents the passion of a budding artist.
Skyblue represents creative freedom.
Playful, independent, confident and care-free.
Black reflects Kiki's robotic nature.
Budding floral accents.
"""
# Settings to generate an interesting Kiki quickly.
# Ten steps was as low as I could get for now; might try again
# when we get other schedulers to play with.
SPLASH_PARAMETERS = dict(
    steps=10,
    guidance_strength=4.5,
    scheduler="DDIM",
    eta=1/8,
)


class MainWindow(Ui_MainWindow):
    application: QtWidgets.QApplication
    _preview_decoder = ApproximateDecoder()

    def __init__(self, application):
        self.application = application
        self.widget = QtWidgets.QMainWindow()
        self.engine = QEngine()
        self.application.aboutToQuit.connect(self.engine.thread.quit)
        self.application.setProperty(ENGINE, self.engine)
        self.setupUi(self.widget)
        self.textPromptInput.setPlainText(DEFAULT_PROMPT)
        self._connect_signals()

        self.monitor = MonitorView(self.graphicsView)

        if has_pyqtgraph():
            self.graphs = GraphWindow(application)
            self.graphs.show()

        QTimer.singleShot(0, self.pipeline_readiness_frame.do_load_pipeline)

    def _connect_signals(self):
        self.generate_button.clicked.connect(self.generate_image)
        self.actionSomething.triggered.connect(self.debug_menu)
        self.actionUnload.triggered.connect(self.engine.do_unload_pipeline)

        self.engine.pipeline_complete.connect(self.set_result)
        self.engine.model_changed.connect(self.prompt_stuff_refresh)
        self.engine.file_saved.connect(self.on_file_saved)
        self.engine.progress_update.connect(self.on_progress_update)

        self.actionQuit.triggered.connect(self.quit)
        QtCore.qInstallMessageHandler(self._handle_qt_message)

    def generate_image(self):
        self.engine.do_generate_from_text.emit(
            self.textPromptInput.toPlainText(),
            self.opposing_text_prompt_input.toPlainText()
        )

    def set_result(self, result: StableDiffusionPipelineOutput):
        # Getting the ndarray of values to load in to a pixmap was unexpectedly difficult.
        # Thankfully qimage2ndarray figured it out.
        # TODO: Is it possible to skip the QImage and load straight to QPixmap?
        q_img: QtGui.QImage = array2qimage(result.images[0], normalize=(0, 1))

        self._set_label_pixmap(self.imageOne, q_img)

        self.complete_progress_bar()

    def show(self):
        self.widget.show()

    def log(self, text):
        self.textBrowser.append(text)

    def on_file_saved(self, path):
        self.log(f"Image saved to {path}")

    def on_progress_update(self, state: IntermediateState):
        self.update_progress_bar(state.step, state.timestep)
        latents = state.latents[0]
        rgb_img, texture = self._preview_decoder(latents)
        self._set_label_pixmap(self.imageOne, rgb_img, scale=8)

        self._update_latent_views(latents)
        self._update_aux_views(latents, texture)

    def _update_aux_views(self, latents: np.ndarray, texture: np.ndarray) -> None:
        """Update whatever extra views we've crammed in here today.

        :param latents: the latent channels
        :param texture: the "extra" channel from hotgrits' rgb transformation.
        """
        # TODO: When it comes to this experimental stuff, it'd probably be better to have a
        #       container we dynamically add views to in code, instead of requiring the ui
        #       file to provide `aux_1`, `aux_2`… etc.

        self._set_label_pixmap(self.channel_T, array2qimage(texture, normalize=(-1, 1)))

        # Simple 1st-order laplacian convolution of the texture channel.
        t_prime = laplace_1(texture)
        self._set_label_pixmap(self.channel_T_prime, array2qimage(t_prime, normalize=(-2, 2)))

        # Second-derivative laplacian filter as implemented by scipy.ndimage.
        aux1 = ndimage.laplace(texture)
        self._set_label_pixmap(self.channel_aux1, array2qimage(aux1, normalize=(-2, 2)))

        # laplacian convolution of all the latent channels (before the rgb+texture transform)
        la_latents = laplace_1(latents)

        # invert some before we colorize
        inverts = [1, 1, -1, -1]
        lalalala = np.einsum('... c h w, c -> ...c h w', np.fabs(la_latents), inverts)
        standardized = standardize(lalalala, scale=2)

        # try using the same 4-channel-to-RGB filter to visualize these.
        # (I'm not convinced it's a good idea, but it was close at hand.)
        aux2 = self._preview_decoder(standardized)[0]
        self._set_label_pixmap(self.channel_aux2, aux2)

        # alternately, view the max of the absolute values. Or should we take the sum?
        #  aux2 = einops.reduce(np.fabs(la_latents), '… c h w -> … h w', 'max')

    def _update_latent_views(self, latents):
        # some channels are easier to interpret when inverted
        inverts = [1, 1, -1, -1]
        latent_labels = self.latents_container \
            .findChildren(QLabel, QRegularExpression(r"channel_\d+"))
        for label, latent, invert in zip(latent_labels, latents, inverts):
            latent_img: QtGui.QImage = array2qimage(latent * invert, normalize=(-1, 1))
            self._set_label_pixmap(label, latent_img)

    def _set_label_pixmap(self, label: QLabel, img: QtGui.QImage, *, scale=1):
        # I tried using label.pixmap() instead of creating a new Pixmap every time,
        # but results ranged from not updating to freezing to SIGSEGV.
        pixmap: QtGui.QPixmap = QtGui.QPixmap(img.size() * scale)
        if pixmap.convertFromImage(img):
            label.setPixmap(pixmap.scaledToWidth(img.width() * scale))
        else:
            self.log("Failed to convert data to pixmap.")

    def update_progress_bar(self, step, timestep):
        self.generating_progress.setEnabled(True)
        self.generating_progress.setMaximum(self.engine.max_timestep)
        # during generation, time flows backwards from 1000 (most diffuse)
        # to 0 (not diffused)
        self.generating_progress.setValue(self.engine.max_timestep - timestep)

    def complete_progress_bar(self):
        self.generating_progress.setMaximum(self.engine.max_timestep)
        self.generating_progress.setValue(self.engine.max_timestep)
        self.generating_progress.setEnabled(False)

    # ######################################################## #
    # prompt and inputs and stuff                              #

    def is_generate_button_enabled(self):
        return self.engine.is_ready()

    def prompt_stuff_refresh(self):
        self.generate_button.setEnabled(self.is_generate_button_enabled())

    def quit(self):
        self.application.quit()

    def debug_menu(self):
        print("Hello are you here?")

    def _handle_qt_message(self, message_type: QtCore.QtMsgType,
                           context: QtCore.QMessageLogContext,
                           message: str):
        # PyQt5 lacks a convenient way to get a string for a QtMsgType.
        # `context` might be useful, but not if it's always just pointing
        # to our excepthook function.
        # self.log(f"{context.file}:{context.line}\t{context.category}\t{context.function}")
        self.log(message)
