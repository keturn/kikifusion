import torch.cuda
from PyQt5.QtChart import QChartView, QLineSeries, QValueAxis, QAreaSeries, QChart
from PyQt5.QtCore import QTimer, QElapsedTimer, QMargins
from PyQt5.QtGui import QFont


def _blank_axis(v_range: tuple = None) -> QValueAxis:
    axis = QValueAxis()
    axis.setLabelsVisible(False)
    axis.setTitleVisible(False)
    axis.setVisible(False)
    axis.setLabelsAngle(90)
    axis.setLabelsFont(QFont())
    if v_range is not None:
        axis.setRange(*v_range)
    return axis


class MonitorView:
    MAX_POINTS = 60
    INTERVAL_MS = 1000

    def __init__(self, chart_view: QChartView):
        self.chart_view = chart_view
        self._init_chart(chart_view.chart())

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_chart)
        self.clock = QElapsedTimer()
        self.clock.start()
        self.update_chart()
        self.timer.start(self.INTERVAL_MS)

    def _init_chart(self, chart):
        # oh geez this is so many lines of code and it doesn't even look good!
        # would it be easier to drop QChart and make a couple polygons myself?
        self.chart: QChart = chart
        self.chart.setObjectName("GPU_Chart")
        self.chart.legend().detachFromChart()
        self.our_memory_usage = QLineSeries(self.chart)
        self.other_memory_usage = QLineSeries(self.chart)
        self.other_edge = QLineSeries(self.chart)
        our_area = QAreaSeries(self.our_memory_usage, None)
        other_area = QAreaSeries(self.other_memory_usage, self.other_edge)
        self.chart.addSeries(our_area)
        self.chart.addSeries(other_area)
        self.our_area = our_area

        self.x_axis = _blank_axis()
        self.chart.setAxisX(self.x_axis, our_area)
        self.chart.setAxisX(self.x_axis, other_area)

        self.device_max_memory = torch.cuda.mem_get_info()[1]
        our_y = _blank_axis((0, self.device_max_memory))
        self.chart.setAxisY(our_y, our_area)

        other_y = _blank_axis((0, self.device_max_memory))
        other_y.setReverse(True)
        self.chart.setAxisY(other_y, other_area)

        self.chart.legend().setVisible(False)
        # self.chart.setBackgroundVisible(False)
        self.chart.setMargins(QMargins())
        self.chart.setBackgroundRoundness(0)

        # self.chart.setPlotArea(QRectF(0, 0, self.chart_view.width(), self.chart_view.height()))

    def update_chart(self):
        now = self.clock.elapsed()
        free, max_memory = torch.cuda.mem_get_info()

        stats = torch.cuda.memory_stats()

        # TODO: I don't know how to get the numbers reported by pytorch to reconcile with those
        #     reported by nvidia-smi. This calculation leaves us trying to shirk the blame for
        #     some fraction of our memory usage. 🙁
        our_allocation = stats["reserved_bytes.all.current"]
        # our_allocation = stats["allocated_bytes.all.current"]
        # active = stats["active_bytes.all.current"]
        # inactive = stats["inactive_split_bytes.all.current"]

        total_used = max_memory - free
        other_memory_usage = total_used - our_allocation

        if len(self.our_memory_usage) > self.MAX_POINTS:
            self.our_memory_usage.remove(0)
            self.other_memory_usage.remove(0)
            self.other_edge.remove(0)

        self.our_memory_usage.append(now, our_allocation)
        self.other_memory_usage.append(now, other_memory_usage)
        self.other_edge.append(now, 0)

        self.x_axis.setRange(now - self.INTERVAL_MS * self.MAX_POINTS, now)

        self.chart.setTitle(f"{100 * free / max_memory:.0f}%")
