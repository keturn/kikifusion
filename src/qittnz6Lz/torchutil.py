import time
from datetime import timedelta

import torch.cuda


class HowMuchMemory:
    memory_before = None
    memory_after = None
    time_before = None
    time_after = None

    def __enter__(self):
        torch.cuda.reset_peak_memory_stats()
        self.memory_before = torch.cuda.max_memory_allocated()
        self.time_before = time.perf_counter()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.memory_after = torch.cuda.max_memory_allocated()
        self.time_after = time.perf_counter()

    def memory_used(self):
        return self.memory_after - self.memory_before

    def time_elapsed(self):
        return timedelta(seconds=self.time_after - self.time_before)


def sizeof_fmt(num, suffix="B"):
    # thanks https://stackoverflow.com/a/1094933
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:,.1f}Yi{suffix}"
