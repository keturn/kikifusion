"""Event-driven interface to the image generation engine."""
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import numpy as np
import torch
from PyQt5.QtCore import QObject, pyqtSignal, QThread
from diffusers.pipelines.stable_diffusion import StableDiffusionPipelineOutput

from qittnz6Lz.engine import Engine, Readiness, RandomNamesForUnrelatedBatches
from qittnz6Lz.facehug.generator_pipeline import PipelineIntermediateState
from qittnz6Lz.metadata import PipelineOutputWithMetadata


@dataclass(frozen=True)
class IntermediateState:
    run_id: str
    step: int
    timestep: int
    latents: np.ndarray
    predicted_original: Optional[np.ndarray] = None

    @classmethod
    def from_torch(cls, state):
        this = cls(
            run_id=state.run_id,
            step=int(state.step),
            timestep=int(state.timestep),
            latents=_detach_to_numpy(state.latents),
            predicted_original=(
                _detach_to_numpy(state.predicted_original)
                if state.predicted_original is not None else None
            )
        )
        this.latents.setflags(write=False)
        if this.predicted_original is not None:
            this.predicted_original.setflags(write=False)
        return this


def _detach_to_numpy(tensor: torch.Tensor):
    # TODO: are all these necessary?
    #     numpy() needs cpu(), but unsure if clone and detach are
    #     required to make sure we're fully disentangled (not holding references)
    #     to the original memory.
    return tensor.clone().detach().cpu().numpy()


class QEngine(QObject):
    """Worker thread for image generation.

    This object creates its own thread to run on.

    Inter-thread communication in Qt is done via signals. This defines signals you can emit to
    request actions, and signals you may connect to for notification of the results.
    """

    # signals I subscribe to:    (should these be Actions?)
    do_generate_from_text = pyqtSignal(str, str)
    do_load_pipeline = pyqtSignal()
    do_unload_pipeline = pyqtSignal()

    # emitted signals:
    progress_update = pyqtSignal(IntermediateState)
    pipeline_complete = pyqtSignal(StableDiffusionPipelineOutput)
    model_changed = pyqtSignal(str)
    file_saved = pyqtSignal(Path)

    max_timestep = 1_000  # at least for the normal Standard Diffusion model

    def __init__(self):
        super().__init__()
        self.thread = QThread()
        self.thread.setObjectName("Diffusion Engine")

        self.moveToThread(self.thread)
        self.engine = Engine(safety_check=False)
        self.do_load_pipeline.connect(self._load)
        self.do_unload_pipeline.connect(self._unload)
        self.do_generate_from_text.connect(self._generate_from_text)
        self.thread.start()

    def _load(self):
        self.engine.load()
        self.model_changed.emit(str(self.engine.pipeline))

    def _unload(self):
        self.engine.unload()
        self.model_changed.emit(None)

    def _generate_from_text(self, prompt: Optional[str], opposing_prompt: Optional[str]):
        process = \
            self.engine.generate_from_text(prompt, opposing_prompt)
        for step_result in process:
            if isinstance(step_result, PipelineIntermediateState):
                # we're sending this data out of thread; make sure it is a clean copy
                # not tied to this thread's gpu data.
                self.progress_update.emit(IntermediateState.from_torch(step_result))
            else:
                self.pipeline_complete.emit(step_result)
                self._autosave_pipeline_output(step_result)

    def is_uninitialized(self):
        return self.engine.readiness == Readiness.UNINITIALIZED

    def is_loading(self):
        return self.engine.readiness == Readiness.IN_TRANSITION

    def is_ready(self):
        return self.engine.readiness == Readiness.READY

    def _autosave_pipeline_output(self, result: PipelineOutputWithMetadata):
        output_path = Path("output/")  # TODO: setting
        opener = RandomNamesForUnrelatedBatches(output_path)
        saved = self.engine.save_output_image(result, opener)
        for path in saved:
            self.file_saved.emit(path)
