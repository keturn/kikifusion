import transformers


class NullFeatureExtractor(transformers.FeatureExtractionMixin):
    def __bool__(self):
        return False
