from numbers import Real
from typing import Union

import einops
import numpy as np
from numpy import newaxis
from scipy.signal import convolve

laplacian_3 = np.array([
    [ 1, -2,  1],
    [-2,  4, -2],
    [ 1, -2,  1],
])
laplacian_3.setflags(write=False)


def laplace(a: np.ndarray):
    kernel = laplacian_3
    if a.ndim > 2:
        kernel = einops.repeat(kernel, 'i j -> c i j', c=a.shape[0])
    return convolve(a, kernel, mode='valid')


def standardize(array: np.ndarray, scale: Union[np.number, Real] = 1) -> np.ndarray:
    """
    Standardize each 2D channel according to its own mean and standard deviation.

    (Is this not implemented in numpy or scipy already?)
    """
    deviations: np.ndarray = np.std(einops.rearrange(array, '... c h w -> ... c (h w)'), axis=-1)
    # std() must necessarily have calculated the mean, is there a way to avoid re-calculating it?
    means: np.ndarray = einops.reduce(array, '... c h w -> ... c', 'mean')

    # einsum doesn't have elementwise addition: https://github.com/numpy/numpy/issues/8139
    centered = array - means[..., newaxis, newaxis]

    standardized: np.ndarray = np.einsum(
        '... c h w, ... c -> ... c h w',
        centered, 1 / (deviations * scale)
    )
    return standardized
