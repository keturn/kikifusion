import signal
import sys
from traceback import format_exception

from PyQt5.QtCore import qCritical


def excepthook_log_as_critical(exctype, value, traceback):
    sys.stderr.writelines(format_exception(exctype, value, traceback))
    qCritical('\n'.join(format_exception(exctype, value, traceback)))


def main():
    # Allow Ctrl-C to work while still in the Qt main loop
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    from PyQt5 import QtWidgets
    from qittnz6Lz.app import MainWindow

    # Replace PyQt5's execpthook with one that doesn't abort the process.
    sys.excepthook = excepthook_log_as_critical

    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow(app)
    w.show()
    return app.exec()


if __name__ == "__main__":
    main()
