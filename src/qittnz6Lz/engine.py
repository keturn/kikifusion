"""Front-end independent for image generation.

Please avoid adding Qt code to this module.
"""
import gc
import secrets
from enum import Enum
from functools import wraps
from pathlib import Path
from typing import Optional

import torch
from diffusers import DDIMScheduler
from diffusers.pipelines.stable_diffusion import StableDiffusionPipelineOutput

from .facehug.generator_pipeline import StableDiffusionGeneratorPipeline, PipelineIntermediateState
from .facehug.null_feature_extractor import NullFeatureExtractor
from .metadata import PipelineOutputWithMetadata
from .torchutil import HowMuchMemory, sizeof_fmt

# STABLE_DIFFUSION_VERSION = "CompVis/stable-diffusion-v1-4"
STABLE_DIFFUSION_VERSION = "runwayml/stable-diffusion-v1-5"


class Readiness(Enum):
    UNINITIALIZED = 0
    READY = 1
    IN_TRANSITION = 2
    BUSY = 3


def transitioning_to(next_state: Readiness):
    # okay this is a bit too much nesting, we should use a state machine helper someone has
    # already thought through.
    def wrapper(method):
        @wraps(method)
        def in_transition(self, *a, **kw):
            self.readiness = Readiness.IN_TRANSITION
            try:
                result = method(self, *a, **kw)
                self.readiness = next_state
                return result
            except Exception:
                self.readiness = Readiness.UNINITIALIZED
                raise
        return in_transition
    return wrapper


def busy(method):
    @wraps(method)
    def busy_working(self, *a, **kw):
        previous_state = self.readiness
        self.readiness = Readiness.BUSY
        try:
            return method(self, *a, **kw)
        finally:
            self.readiness = previous_state
    return busy_working


class Engine:
    ATTENTION_SLICES = 2  # TODO: #11, #20

    pipeline: Optional[StableDiffusionGeneratorPipeline]

    jpeg_settings = dict(
        quality=95,
        optimize=True,
        subsampling='4:4:4'
    )

    def __init__(self, *, safety_check=True):
        self.readiness = Readiness.UNINITIALIZED
        self.pipeline = None
        self.safety_check = safety_check

    @transitioning_to(next_state=Readiness.READY)
    def load(self):
        pretrained_args = dict(
            use_auth_token=True,
            revision="fp16", torch_dtype=torch.float16,
            # TODO: local_files_only=True
        )
        pipeline_args = dict(
            # scheduler=diffusers.LMSDiscreteScheduler(
            #     beta_start=0.00085, beta_end=0.012, beta_schedule="scaled_linear"),
            scheduler=DDIMScheduler(beta_start=0.00085, beta_end=0.012,
                                    beta_schedule="scaled_linear", clip_sample=False,
                                    set_alpha_to_one=False)
        )

        mem = HowMuchMemory()
        with mem:
            if not self.safety_check:
                # standard: loads 18s, using 2.6GiB vRAM
                # unsafe  : loads 13s, using 2.0GiB vRAM
                # meaning safety checker costs +40% load time and +30% base RAM
                pipeline_args.update(
                    feature_extractor=NullFeatureExtractor(),
                    safety_checker=None
                )
            from transformers import CLIPTextModel
            # TODO: explicitly loading the text_encoder before the pipeline was the best way I found
            #     to keep it on the CPU, but there might be better options after accelerate
            #     integration is merged: https://github.com/huggingface/diffusers/pull/361
            text_encoder = CLIPTextModel.from_pretrained(
                STABLE_DIFFUSION_VERSION,
                subfolder="text_encoder", device_map=dict(text_model="cpu", text_encoder="cpu", other="cuda"),
                **pretrained_args,
            )
            pipe = StableDiffusionGeneratorPipeline.from_pretrained(
                STABLE_DIFFUSION_VERSION,
                text_encoder=text_encoder,
                **pretrained_args,
                **pipeline_args
            )
            pipe.unet.to("cuda")  # unet does all the heavy lifting, it goes to GPU
            pipe.vae.to("cuda")   # vae's vRAM requirements are relatively modest
            # notably we are _not_ moving text_encoder to GPU. Leaving it on CPU
            # spares several hundred MB of vRAM, and it's fast enough on CPU.

        self._log(f"Pipeline loaded in {mem.time_elapsed()}, "
                  f"using {sizeof_fmt(mem.memory_used())} vRAM.")

        pipe.enable_attention_slicing(self.ATTENTION_SLICES)

        self.pipeline = pipe

    @transitioning_to(next_state=Readiness.UNINITIALIZED)
    def unload(self):
        self.pipeline = None
        gc.collect()  # clean up old Python allocations right away
        torch.cuda.empty_cache()

    @busy
    def generate_from_text(self, prompt: str, opposing_prompt: str) -> StableDiffusionPipelineOutput:
        steps = 10

        process = self.pipeline.generate(
            prompt=prompt,
            opposing_prompt=opposing_prompt,
            # output_type='nd.array',  # required for stock diffusers 0.3 pipelines
            num_inference_steps=steps
        )
        for step_result in process:
            if isinstance(step_result, PipelineIntermediateState):
                yield step_result
            elif isinstance(step_result, StableDiffusionPipelineOutput):
                result = PipelineOutputWithMetadata.augment_output(step_result, self.pipeline)
                result.prompts = [prompt]
                result.opposing_prompts = [opposing_prompt] if opposing_prompt else None
                result.steps = steps
                # FIXME: result seed, guidance strength, width, height
                #     https://github.com/huggingface/diffusers/issues/526
                yield result
            else:
                self._log(f"unhandled pipeline output of type {type(step_result)}")

    def save_output_image(self, pipeline_output: PipelineOutputWithMetadata, path_provider):
        images = self.pipeline.numpy_to_pil(pipeline_output.images)
        outputs = []
        for index, image in enumerate(images):
            path = path_provider(index)
            with path.open('xb') as output_file:
                image.save(output_file, 'jpeg', exif=pipeline_output.as_exif(), **self.jpeg_settings)
            outputs.append(path)
        return outputs

    def _log(self, msg):
        print(msg)


class RandomNamesForUnrelatedBatches:
    output_path: Path
    ID_LENGTH = 8

    def __init__(self, output_path, prefix="t", suffix='.jpg'):
        self.output_path = output_path
        self.image_id = prefix + secrets.token_urlsafe(self.ID_LENGTH)
        self.suffix = suffix

    def __call__(self, index=None):
        stem = self.image_id
        if index is not None and index > 0:
            stem = f"{stem}{index:03d}"
        return (self.output_path / stem).with_suffix(self.suffix).resolve()
