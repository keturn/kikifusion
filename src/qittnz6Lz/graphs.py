import numpy as np
import pyqtgraph.opengl as qggl
from PyQt5 import QtWidgets
from pyqtgraph import Vector, ImageView

from .constants import ENGINE
from .generated.graphwindow import Ui_GraphWindow
from .qengine import IntermediateState


class GraphWindow(Ui_GraphWindow):
    def __init__(self, application: QtWidgets.QApplication):
        self.application = application
        self.widget = QtWidgets.QMainWindow()
        self.setupUi(self.widget)

        blank_data = np.zeros((64, 64), dtype=np.float)

        self.image_view = ImageView(parent=self.centralwidget)
        self.verticalLayout.addWidget(self.image_view)

        self.surface_plot = qggl.GLSurfacePlotItem(z=blank_data, shader='heightColor')
        self.gl_view.addItem(self.surface_plot)

        self._connect_signals()

    def _connect_signals(self):
        engine = self.application.property(ENGINE)
        engine.progress_update.connect(self.on_progress_update)

    def show(self):
        self.widget.show()

    def on_progress_update(self, state: IntermediateState):
        latents = state.latents[0]

        if state.step is None or state.step == 0:
            height, width = latents[0].shape
            self.gl_view.opts['center'] = Vector(height / 2, width / 2)

        self.image_view.setImage(latents[1].transpose())
        self.surface_plot.setData(z=latents[1])
