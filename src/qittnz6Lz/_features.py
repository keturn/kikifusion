def has_pyqtgraph():
    try:
        import pyqtgraph
        import pyqtgraph.opengl
    except ImportError:
        return False
    return True
