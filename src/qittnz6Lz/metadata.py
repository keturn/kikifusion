import json
from dataclasses import dataclass
from typing import List, Dict, Any

import piexif
from diffusers import DiffusionPipeline
from diffusers.pipelines.stable_diffusion import StableDiffusionPipelineOutput
from piexif import ImageIFD, ExifIFD
from piexif.helper import UserComment

IFD_STRING_ENCODING = 'latin1'


# noinspection PyPep8Naming
def _lossy_encode_IFD_string(prompt, encoding=IFD_STRING_ENCODING):
    # IFD doesn't handle multibyte characters in ASCII fields.
    # noinspection PyUnresolvedReferences
    import translitcodec  # must be imported to register codecs
    return prompt.encode(encoding, 'replace/translit/long').decode(encoding)


@dataclass
class PipelineOutputWithMetadata(StableDiffusionPipelineOutput):
    """Pipeline Output with input parameters attached.

    terrible kludgy thing until we get support upstream
    https://github.com/huggingface/diffusers/issues/526
    """

    prompts: List[str]
    opposing_prompts: List[str]
    steps: int
    scheduler: str
    scheduler_args: Dict[str, Any]
    seeds: List[int]
    width: int
    height: int
    software: str

    # noinspection PyTypeChecker
    @classmethod
    def augment_output(cls, output: StableDiffusionPipelineOutput, pipe: DiffusionPipeline) \
            -> "PipelineOutputWithMetadata":
        output.__class__ = cls
        cls.set_from_pipeline(output, pipe)
        return output

    def set_from_pipeline(self, pipe: DiffusionPipeline) -> "PipelineOutputWithMetadata":
        config = pipe.config
        self.scheduler = ".".join(config["scheduler"])
        self.software = f"diffusers {config['_diffusers_version']}"
        # ('vae', ('diffusers', 'AutoencoderKL')),
        # ('_class_name', 'WoStableDiffusionPipeline'),
        # ('text_encoder', ('transformers', 'CLIPTextModel')),
        # ('tokenizer', ('transformers', 'CLIPTokenizer')),
        # ('unet', ('diffusers', 'UNet2DConditionModel')),
        # ('scheduler', ('diffusers', 'DDIMScheduler')),
        # ('safety_checker', ('stable_diffusion', 'StableDiffusionSafetyChecker')),
        # ('feature_extractor', ('transformers', 'CLIPFeatureExtractor'))])
        return self

    def metadata(self) -> Dict[str, Any]:
        metadata = dict(
            prompt=self.prompts[0],  # TODO: handle multi-prompt batches
            sampler=self.scheduler,
            steps=self.steps,
        )
        if self.opposing_prompts and any(self.opposing_prompts):
            metadata.update(
                opposing_prompt=self.opposing_prompts[0]
            ) 
        return metadata

    def as_json(self) -> str:
        return json.dumps(self.metadata(), ensure_ascii=False)

    def as_exif(self) -> bytes:
        prompt = self.prompts[0]  # TODO: handle multi-prompt batches
        exif_dict = {
            '0th': {
                ImageIFD.ImageDescription: _lossy_encode_IFD_string(prompt),
                ImageIFD.Software: self.software,
            },
            'Exif': {
                ExifIFD.UserComment: UserComment.dump(self.as_json(), encoding="unicode"),
            }
        }
        # other potential options include:
        # the XMP Dublin Core properties https://developer.adobe.com/xmp/docs/XMPNamespaces/dc/
        return piexif.dump(exif_dict)
