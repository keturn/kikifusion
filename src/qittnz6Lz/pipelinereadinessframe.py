from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QFrame, QApplication

from qittnz6Lz.constants import ENGINE
from qittnz6Lz.generated.readinessframe import Ui_PipelineReadinessFrame
from qittnz6Lz.qengine import QEngine


class PipelineReadinessFrame(QFrame, Ui_PipelineReadinessFrame):
    def __init__(self, parent):
        super().__init__(parent)
        self._engine: QEngine = QApplication.instance().property(ENGINE)
        self.setupUi(self)
        self._connect_signals()

    def _connect_signals(self):
        self._engine.model_changed.connect(self.pipeline_readiness_refresh)
        self.pushButton.clicked.connect(self.do_load_pipeline)

    def do_load_pipeline(self):
        self._engine.do_load_pipeline.emit()
        # timer kludge to cover up for the fact that we don't have data binding that recognizes
        # when engine.readiness changes.
        QTimer.singleShot(10, self.pipeline_readiness_refresh)

    def is_pipeline_readiness_frame_visible(self):
        return self._engine.is_uninitialized() or self._engine.is_loading()

    def is_pipeline_button_enabled(self):
        return self._engine.is_uninitialized()

    def is_pipeline_readiness_progress_bar_active(self):
        return self._engine.is_loading()

    def get_pipeline_readiness_text(self):
        if self._engine.is_uninitialized():
            return "Model not loaded."
        elif self._engine.is_loading():
            return "Loading model…"
        else:
            return "Model loaded and ready."

    def pipeline_readiness_refresh(self):
        self.setVisible(self.is_pipeline_readiness_frame_visible())

        self.pipeline_readiness_text.setText(self.get_pipeline_readiness_text())
        if self.is_pipeline_readiness_progress_bar_active():
            # Loading doesn't give progress updates, so spin by setting maximum to 0.
            self.pipeline_readiness_progress.setMaximum(0)
            self.pipeline_readiness_progress.setEnabled(True)
        else:
            self.pipeline_readiness_progress.setMaximum(1)
            self.pipeline_readiness_progress.setEnabled(False)

        self.pushButton.setEnabled(self.is_pipeline_button_enabled())
