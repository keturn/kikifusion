from pathlib import Path

TOP_PACKAGE_DIR = Path("src/qittnz6Lz")
GENERATED_DIR = TOP_PACKAGE_DIR / "generated"
UI_DIR = Path("ui")
MAIN_MODULE_NAME = "qittnz6Lz.main"

UI_COMPILE = "pyuic5"


DOIT_CONFIG = dict(
    default_tasks=["start"]
)


def compile_ui(changed):
    from PyQt5 import uic
    for ui_file in changed:
        relative_input = Path(ui_file).relative_to(UI_DIR)
        output_path = GENERATED_DIR / relative_input.with_suffix(".py")
        with open(output_path, 'w') as output:
            uic.compileUi(ui_file, output)


def _main():
    import qittnz6Lz.main
    qittnz6Lz.main.main()


def _rigged():
    import jurigged
    jurigged.watch(str(TOP_PACKAGE_DIR))
    _main()


def task_compile_ui():
    try:
        from PyQt5 import uic
    except ImportError as e:
        return dict(
            actions=[],
            doc=f"⚠ Qt uic module not available: {e}",
        )

    for ui_path in UI_DIR.glob("*.ui"):
        relative = ui_path.relative_to(UI_DIR)
        yield dict(
            basename="compile-ui",
            name=relative.stem,
            actions=[compile_ui],
            file_dep=[ui_path],
            targets=[GENERATED_DIR / relative.with_suffix(".py")]
        )


def task_start():
    return dict(
        actions=[_main],
        task_dep=["compile-ui"],
        verbosity=2
    )


def task_rigged():
    return dict(
        actions=[_rigged],
        task_dep=["compile-ui"],
        verbosity=2,
    )
